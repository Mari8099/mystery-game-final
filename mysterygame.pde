// Just some variables 
int Screen = 0;
int Count = 0;
import processing.sound.*;
SoundFile fl;
Back b = new Back();
Back b1 = new Back();
int LED = 18;
import processing.io.*;
// image and font variables
PImage ig;
PImage ig1;
PImage ig2;
PImage ig3;
PImage ig5;
PImage ig6;
PImage ig7;
PImage ig8;
PImage ig9;
PImage ig10;
PImage ig11;
PImage ig12;
PFont f;
PFont f1;
// creating the setup for the background 
void setup () {
  size(1350,700);
//bakcground 
  ig=loadImage("pxArt (5).png");
  ig1=loadImage("image.jpg");
  ig2=loadImage("pxArt (6).png");
  ig3= loadImage("donut4.png");
  // dogs 
  ig5 = loadImage("Dog1.png");
  ig6 = loadImage("Dog2.png");
//clues 
  ig7 = loadImage("Clue1.png");
  ig8 = loadImage("Clue2.png");
  ig9 = loadImage("Clue3.png");
  ig10 = loadImage("Clue4.png");
  ig11= loadImage("Clue5.png");
  ig12= loadImage("youwin.jpg");
//used sound library
  fl = new SoundFile(this, "audio1.mp3");
  fl.play();
  fl.amp(.1);
 // led 
GPIO.pinMode(LED, GPIO.OUTPUT);

 }
// what will be draw on the background plus the different screens 
void draw () {
  background(0);
  b.fonts();
  if (Screen == 0) {
  b.display();
  b.d();
  textFont(f1, 100);
  fill(#FFFFFF);
  text("Press ENTER to Start", 330, 500);
  text("The Missing Donut", 375, 120);
} else if (Screen == 1) {
  b.display();
  b.textbox();
  b.click();
  textFont(f,75);
  fill(#FFFFFF);
  text("There was an incident on third street.", 280, 600);
} else if (Screen == 2){
  b.display();
  b.textbox();
  b.click();
  textFont(f,75);
  fill(#FFFFFF);
  text("The cops were called at 3:25 am", 280, 600);
} else if (Screen == 3) {
  b.display();
  b.textbox();
  b.click();
  textFont(f,75);
  fill(#FFFFFF);
  text("There were only two dogs at the scene..", 280, 600);
} else if (Screen == 4) {
  b.display();
  b.textbox();
  b.click();
  textFont(f,65);
  fill(#FFFFFF);
  text("Help find all five clues to find out what happened..", 280, 600);  
}
  if (Screen == 5) {
  image(ig1,0,-350);
  textFont(f1,65);
  fill(#5D757E);
  text("FIND ALL FIVE CLUES", 455, 100); 
  text("Use the mouse to click on clues",455,225);
  image(ig5, 600,500);
  image(ig6, 250, 500);
  if(object1) {
    showClue1();
  }
  if(object2){
  showClue2 ();
  }
  if(object3){
  showClue3 ();
  }
  if(object4){
  showClue4 ();
  }
if(object5){
  showClue5 ();
}
if(Count == 0){
  textFont(f1, 50);
  fill(#3CDE83);
  text("Clues Found =0",455,150);
}else if(Count == 1) {
  textFont(f1, 50);
   fill(#3CDE83);
  text("Clues Found =1",455, 150);
}else if(Count ==2) {
  textFont(f1, 50);
   fill(#3CDE83);
  text("Clues Found =2", 455, 150);
}else if (Count == 3){
  textFont(f1,50);
   fill(#3CDE83);
  text("Clues Found =3", 455, 150);
}else if(Count == 4){
  textFont(f1, 50);
   fill(#3CDE83);
  text("Clues Found =4", 455, 150);
}else if(Count == 5){
  image(ig12,350, 200);
  textFont(f1 ,50);
  fill(#4A59D8);
  text("With all clues found the one who is guilty is...", 300, 450);
  text("The Chihuahua?!", 485, 500); 
}
}
}
 void showClue1() {
image(ig7,200,100);
}
void showClue2() {
  image(ig8, 75, 600);
}
void showClue3() {
  image(ig9,850,450);
}
void showClue4() {
 image(ig10,550,250);
}
void showClue5() {
  image(ig11,900,0);
}
//Creating a class for my backgrounds 
class Back{
 Back(){
 }
 float x = 555;
 float y = 250;
void display(){
  image(ig,0,0);
}
void textbox(){
  fill(#5253A5);
  rect(width/5,500, width-200,150);
}
void click(){
  fill(#FFFFFF);
  textFont(f1,100);
  text("Press ENTER to continue",320,120);
}
void fonts() {
  f1 = loadFont("AgencyFB-Bold-48.vlw"); 
  f = loadFont ("AgencyFB-Reg-48.vlw");
}
void d() {
  y--;
  x = x + random(0,0);
  image(ig3, x,y);
}
}
float rX= 250, rY=100, rW=50, rH=50;
float rX1=105, rY1=600, rW1=50, rH1=50;
float rX2=950, rY2=450, rW2=50, rH2=50;
float rX3=550, rY3=250, rW3=50, rH3=50;
float rX4=900, rY4=0, rW4=50, rH4=50;
void mousePressed() {
if (mouseX > rX && mouseX < rX + rW1 && mouseY > rY && mouseY < rY + rH) {
 object1 = false;
 Count ++;
 GPIO.digitalWrite(LED, GPIO.LOW);
}if(mouseX > rX1 && mouseX < rX1 + rW1 && mouseY > rY1 && mouseY <rY1 + rH1) {
  object2 = false;
 Count ++;
 GPIO.digitalWrite(LED, GPIO.HIGH);
} if(mouseX > rX2 && mouseX < rX2 + rW2 && mouseY > rY2 && mouseY < rY2 + rH2) {
  object3 = false;
  Count++;
   GPIO.digitalWrite(LED, GPIO.LOW);
} if(mouseX > rX3 && mouseX < rX3 + rW3 && mouseY > rY3 && mouseY < rY3 + rH3) {
  object4 = false;
  Count++;
  GPIO.digitalWrite(LED, GPIO.HIGH);
} if(mouseX > rX4 && mouseX < rX4 + rW4 && mouseY > rY4 && mouseY < rY4 + rH4) {
  object5 = false;
  Count++;
  GPIO.digitalWrite(LED, GPIO.LOW);
}
}
void LEDS(){
}
boolean object1 = true;
boolean object2 = true;
boolean object3 = true;
boolean object4 = true;
boolean object5 = true;
boolean led = false;
void keyTyped() { 
   if (Screen == 0) {
     Screen ++;
   } else if(Screen == 1){
     Screen ++;
   } else if (Screen == 2){
     Screen ++;
   } else if (Screen == 3){
     Screen ++;
   } else if (Screen == 4) {
     Screen ++;
   } 
}
